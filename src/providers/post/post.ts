import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';


const endpoints = {
  posts: 'https://jsonplaceholder.typicode.com/posts/'
};

@Injectable()
export class PostProvider {

  constructor(public http: Http) {
    console.log('Hello PostProvider Provider');
  }


  doListPosts() : Observable<any> {
    return this.http.get( endpoints.posts );
  }

}
