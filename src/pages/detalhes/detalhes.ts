import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Post } from '../../models/Post';

/**
 * Generated class for the DetalhesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalhes',
  templateUrl: 'detalhes.html',
})
export class DetalhesPage {

  post: Post;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {
    // console.log(this.navParams.get('post'));
    this.post = <Post> this.navParams.get('post');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalhesPage');
  }

  close(){
    this.viewCtrl.dismiss();
  }

}
