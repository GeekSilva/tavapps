import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController } from 'ionic-angular';
import { Post } from '../../models/Post';
import { PostProvider } from '../../providers/post/post';
import { DetalhesPage } from '../detalhes/detalhes';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts: Post[] = [];

  constructor(public navCtrl: NavController, private postProvider: PostProvider, private modalCtrl: ModalController, private loadingCtrl: LoadingController) {
    let loading = this.loadingCtrl.create({
      content: ''
    });
    loading.present();
      this.postProvider.doListPosts().subscribe(data => {
        this.posts = <Post[]> data.json();
        loading.dismiss();
      }, err => {
        console.log('Houve um erro ao requisitar');
        loading.dismiss();
      });
  }

  detalhes(post: Post) {
    this.modalCtrl.create( DetalhesPage, {
      post: post
    }).present();
  }



}
